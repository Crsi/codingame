n = int(input())
y = 1
found = 0
lines = ""
for i in range(n):
    row = input()
    lines += row
    x = 1
    for c in row:
        if c in 'x+o':
            print("(",x,",",y,")", sep = "")
            found += 1
        x += 1
    y += 1

if not found:
    if not len(lines.replace('#','')):
        print("BLINDED")
    else:
        print("SEARCH")

