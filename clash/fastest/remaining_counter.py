import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

c, s = input().split()

# Write an answer using print
# To debug: print("Debug messages...", file=sys.stderr)

def f(x):
    y=x.split(":")
    return int(y[0])*60+int(y[1])

cur = f(c)
stp = f(s)

diff = stp - cur

print(diff, file = sys.stderr)

m = 0
while diff > 60:
    diff -= 60
    m += 1

diff = str(diff)
if len(diff) == 1:
    diff = "0" + diff

print(f"{m}:{diff}")
