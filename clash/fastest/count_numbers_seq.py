x = input()
while len(x) > 0:
    c = x[0]
    s = 0
    while x.startswith(c):
        s += 1
        x = x[1:]
    print(s, c, sep = "", end = "")

