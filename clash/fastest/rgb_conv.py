r = int(input())
g = int(input())
b = int(input())
r = hex(r)[2:].upper()
g = hex(g)[2:].upper()
b = hex(b)[2:].upper()
if len(r) == 1: r="0"+r
if len(g) == 1: g="0"+g
if len(b) == 1: b="0"+b
print("#", r, g, b, sep = "")
