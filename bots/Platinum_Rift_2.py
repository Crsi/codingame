#!/usr/bin/env python3

import os
import sys
import json
import math
import time
import queue
import random
import typing
import functools


# IMPLEMENTED IDEAS:
# * Use a rapid random exploration in the first phase of the game.
# * Use a class Environment instead of all those global variables.

# NEW IDEAS:
# * Use a class Squad that simulates a group of PODs
#   so that they can get and fulfill better "quests" like
#   exploring in a BF search tree or directly attacking the
#   enemy HQ without any sense for survival (reckless attack).
#   However, always avoid collisions with other Squads.
# * Rapid exploration: If there are no tiles around that I don't own,
#   the PODs currently do pretty bad movements. Better way: Use one
#   random movement with all PODs (without splitting) and then perform
#   as usual in the next game turn (so that it's not fully randomly).
# * Find and protect the "core area" of the map where two parts of the
#   graph are joint together in one single point. Use a defending Squad
#   for this purpose. But how could the core area be determined?
# * Always have at least 4 PODs in the HQ! If it's under attack,
#   we would survive at least one game turn. After that, we can
#   try to collect even more PODs in the HQ until the enemy dies.
#   However, this should not happen in the first two game turns.
# * Tranform a collected defending Squad into a Squad that attacks
#   the enemy HQ so that we have a lot of power in just one turn.

# Use the rapid exploring technique until this lap
RAPID_EXPLORING = 50

# With this chance, some random game turn will be a rapid
# exploring turn as long as it is no full attack game turn
RAPID_EXPLORING_CHANCE = 0.2

# Use the full attack technique only after this lap
FULL_ATTACK = 450

# With this chance, some random game turn will be a full
# attack turn as long as it is not a safe rapid exploring turn
FULL_ATTACK_CHANCE = 0.25

# Use at least so many PODs for the full attack (per tile)
FULL_ATTACK_MIN_PODS = 5

# Static values for altering the comparision algorithm to sort target tiles
COMP_BASE_VALUE = 0     # Base value of any comparision
COMP_PLATINUM_MULT = 2  # Multiplier for near platinum
COMP_STRONG_ENEMY = -2  # Bonus for near enemies stronger than me
COMP_EQUAL_ENEMY = 0    # Bonus for near enemies equally strong
COMP_WEAK_ENEMY = 4     # Bonus for near enemies weaker than me
COMP_ENEMY_TILE = 4     # Bonus for near tiles belonging to the enemy
COMP_NEUTRAL_TILE = 2   # Bonus for near tiles belonging to noone
COMP_MY_OWN_TILE = -2   # Bonus for near tiles belonging to me
COMP_TEAM_MATES = -4    # Bonus for near team mates on other tiles
COMP_ENEMY_HQ = 10      # Bonus for the target tile being the enemy HQ

# Starting lap for the HQ protection program
HQ_PROTECTION_START_LAP = 20

# Number of laps a Squad with an invalid quest will wait before destroying itself
SQUAD_INVALID_QUEST_WAIT = 4

# Minimum number of unallocated PODs before a new Squad will be spawned
SQUAD_MIN_UNALLOCATED = 5

# Number of PODs of a defending Squad that rest on the target Tile
SQUAD_DEFENSE_DEFAULT_REST = 4

# Maximum number of new hired members for a Squad
SQUAD_MAX_HIRE_PER_TURN = 6

# Maximum number of tries to hire new members for a Squad
SQUAD_MAX_HIRE_TRIES = 5

# Range of laps in which all existing Squads switch to attacking the enemy HQ
SQUAD_ALL_ATTACKING_LAPS = (450, 470)

# Maximum number of Squads performing the given quest
SQUAD_PER_QUEST_MAX = {
    0: 5,  # collector
    1: 1,  # defender
    2: 3,  # attacker
    3: 5,  # mover
    4: 3,  # explorer
    5: 1,  # destroyer (silent)
    6: 1   # destroyer (explosive)
}

# Default strength value for new Squads of the given quest
SQUAD_DEFAULT_STRENGTH = {
    0: (4, 4),   # collector
    1: (4, 12),  # defender
    2: (4, 12),  # attacker
    3: (1, 4),   # mover
    4: (1, 4),   # explorer
    5: (0, 12),  # destroyer (silent)
    6: (0, 12)   # destroyer (explosive)
}

# First game turn where a Squad may be spawned
SQUAD_FIRST_BIRTHDAY = {
    0: 20,  # collector
    1: 1,   # defender
    2: 10,  # attacker
    3: 10,  # mover
    4: 1,   # explorer
    5: 10,  # destroyer (silent)
    6: 10   # destroyer (explosive)
}

# Logging function for sys.stderr
error = functools.partial(print, file = sys.stderr)

# Define the Tasks and Moveables types that's commonly used to plan movements
Moveables = typing.List[int]
Tasks = typing.List[typing.List[int]]


class SquadNeedsTarget(Exception):
    """
    Exception if a Squad has no target but needs one
    """


class SquadNeedsParam(Exception):
    """
    Exception if a Squad has no parameter but needs one
    """


class Tile:
    def __init__(self, tileID: int, player: int, platinum: int, links: list):
        self.tileID = tileID      # ID of this Tile
        self.player = player      # ID of the player controlling the Tile
        self.platinum = platinum  # amount of platinum on this Tile
        self.links = links        # list of links to other tiles
        self.explored = False     # indicator if the Tile was explored (visible)
        self.seen = False         # indicator if the Tile was seen (navigator)
        self.parent = None        # reference to the "parent" zone (navigator)
        self.lastVisit = 0        # number of laps since last visit
        self.my = 0               # number of my own PODs on this Tile
        self.enemy = 0            # number of enemy PODs on this Tile
        self.available = 0        # number of free PODs that can be moved

    def __repr__(self) -> str:
        return "Tile(O: {}, ID: {}, P: {})".format(self.player, self.tileID, self.platinum)

    def __len__(self) -> int:
        return len(self.links)

    def add(self, obj):
        self.links.append(obj)

    def visit(self):
        self.lastVisit = 0

    def lap(self):
        self.lastVisit += 1

    def setPods(self, my: int, enemy: int):
        self.my = my
        self.enemy = enemy
        self.available = self.my


class Squad:
    def __init__(
            self,
            squadID: int,
            strength: typing.Tuple[int, int],
            quest: int,
            param: typing.Optional[typing.Any] = None,
            target: typing.Optional[Tile] = None
    ):
        self.squadID = squadID     # numeric ID of the Squad
        self.strength = strength   # allowed strength of the Squad (limits)

        self.age = 0               # age of the Squad in game turns
        self.quest = quest         # numeric ID of the quest the Squad has
        self.param = param         # parameter for some quests (e.g. distance)
        self.target = target       # Tile of the Squad's target (optional; only some quests)

        self.finished = False      # indicator if the Squad finished its quest
        self.rebuild = False       # indicator if the Squad needs to rebuild its navigation
                                   # map which should be set to True if a new quest was given

        self.members = {}          # dictionary of all allocated PODs for this Squad where
                                   # the key is a tile ID and the value is the number of PODs
        self.history = {}          # history of the members dictionary where the key is the age
                                   # of the Squad and the value is the members dict at this time

    def __repr__(self) -> str:
        return "Squad(ID: {}, C: {} S: {}, Q: {}, T: {})".format(
            self.squadID, self.getStrength(), self.strength, self.quest, self.target
        )

    def _expand(self, tile: Tile, rest: int = -1) -> Tasks:
        """
        Distribute the members on this Tile equally to adjacent Tiles

        If specified, some members could remain on the origin Tile
        to rest there or protect the Tile or something like that.
        """

        if tile.tileID not in self.members:
            error(f"Expand failed. {tile} not in members dict: {self.members}")
            return []

        movers = self.members[tile.tileID]
        if rest < 0:
            rest = SQUAD_DEFENSE_DEFAULT_REST
        movers -= rest
        if movers <= 0:
            return []

        tasks = []
        plans = Environment.get()._planExpand(tile, movers)
        for m in plans:
            if self.members[tile.tileID] >= plans[m]:
                self.members[tile.tileID] -= plans[m]
                if m not in self.members:
                    self.members[m] = 0
                self.members[m] += plans[m]
                tasks.append([plans[m], tile.tileID, m])
            else:
                error(f"Plan of {plans[m]} from {tile.tileId} to {m} failed.")

        return tasks

    def _movePODs(self, src: Tile, dst: Tile, n: int) -> typing.List[int]:
        """
        Move n PODs from source Tile to destination Tile

        If source and destination are no adjacent Tiles,
        the navigation map for this destination will be used
        (maybe from cache), so that the number of PODs will
        be able to move from the source Tile to the "next"
        Tile as stated in the navigation map.
        """

        # Fix and echo problems if there are not enaugh PODs
        # FIXME: This does never work. Allocated PODs are not available!
        #if src.available < n:
        #    n = src.available
        error(f"Moving {n} PODs from {src} to {dst}... Available: {src.available}")

        # Move n PODs from src to dst if they are adjacent
        if src.tileID in dst.links:
            step = dst.tileID

        # Look up the next step on the way in a navigation map otherwise
        else:
            navi = Navigator.navigate(dst.tileID, Environment.get())
            step = navi[src.tileID]

        # Perform the actual movement
        self.members[src.tileID] -= n
        #src.available -= n  # BROKEN FIXME
        if step not in self.members:
            self.members[step] = 0
        self.members[step] += n
        return [n, src.tileID, step]

    def _newTargetCollector(self):
        """
        Reset the target for the collector quest
        """

        def sortPlatinum(tile: Tile):
            return tile.platinum

        targets = []
        env = Environment.get()
        s = sorted(env.tiles.values(), key = sortPlatinum, reverse = True)
        error(f"Sorted: {s}")
        for T in s:
            if T.player != env.myID and T != self.target:
                targets.append(T)
        if len(targets) == 0:
            error("ERROR: No new targets available!")
            targets.append(self.target)
        self.target = targets[0]

    def lap(self):
        """
        Add one game turn to the age of the Squad
        """

        self.age += 1

    def getStrength(self):
        """
        Return the current strength of the Squad
        """

        return sum(self.members.values())

    def collect(self) -> Tasks:
        """
        Collect more Platinum in the given distance from the target
        """

        # TODO
        return []

    def defend(self) -> Tasks:
        """
        Defend the area around the target inside the given distance

        This tactics works as follows: All members of the Squad
        come together in one Tile that must be protected. Once
        all members of this Tile arrived there, they will expand
        equally to all nearby Tiles. In the next game turn, they
        will go back to the target Tile again. So, this is a kind
        of endless loop. Therefore you should usually not have
        more than one or two Squads doing defensive work.
        """

        # If the whole Squad came together in one point,
        # all PODs should equally expand to nearby Tiles
        hits = list(k for k in self.members if k == self.target.tileID)
        together = list(k for k in self.members if self.members[k] > 0)
        if len(together) == 1 and len(hits) == 1:
            if self.param is None:
                return self._expand(self.target, SQUAD_DEFENSE_DEFAULT_REST)
            return self._expand(self.target, self.param)

        # Otherwise the members of the Squad should try
        # to make their way to the target Tile
        else:
            error(f"Defend of Squad {self.squadID} becomes Move")
            return self.move()

    def attack(self) -> Tasks:
        """
        Attack all enemies around (and chase them)
        """

        # TODO
        error("ATTACK not implemented.")
        return []

    def move(self) -> Tasks:
        """
        Move all PODs of the Squad to the target position
        """

        tasks = []
        for P in list(self.members):
            tasks.append(
                self._movePODs(Environment.get().tiles[P], self.target, self.members[P])
            )
        return tasks

    def explore(self) -> Tasks:
        """
        Explore unknown Tiles, then go to Gaia's Tiles, then to enemy's
        """

        # TODO
        error("EXPLORE not implemented.")
        return []

    def destroy(self, expansion: bool) -> Tasks:
        """
        Destroy this Squad so that all PODs are unallocated afterwards

        If the expansion is activated, one game turn is consumed for
        all remaining PODs. They will be spread truly randomly over all
        adjacent Tiles. The PODs will be unallocated in the next game
        turn. Otherwise they will get unallocated in this game turn.
        """

        # TODO
        error(f"DESTROY ({expansion}) not implemented for {self}.")
        return []

    def _fulfill(self) -> Tasks:
        """
        Fulfill the given quest and return the necessary moves
        """

        # Quest: Collect more Platinum almost anywhere on the map
        if self.quest == 0:
            return self.collect()

        # Quest: Defend the area around you
        elif self.quest == 1:
            return self.defend()

        # Quest: Attack any enemies around you
        elif self.quest == 2:
            return self.attack()

        # Quest: Go to the target Tile
        elif self.quest == 3:
            return self.move()

        # Quest: Explore new unknown Tiles, then go to Gaia's Tiles
        elif self.quest == 4:
            return self.explore()

        # Quest: Destroy this squad silently so that all PODs
        # will be unallocated but can be moved in this game turn
        elif self.quest == 5:
            return self.destroy(False)

        # Quest: Destroy this squad in an expansion
        # so that the PODs will make one last move
        elif self.quest == 6:
            return self.destroy(True)

        # Invalid value: Don't do anything
        else:
            error(f"Quest type {self.quest} is not supported!")
            return []

    def _reduce(self) -> Tasks:
        """
        Reduce the number of available PODs below max strength

        This will remove the PODs from the members dict but it
        won't move the PODs around, they will become available only.
        """

        while self.getStrength() > self.strength[1]:
            pre = 0
            selected = None
            for T in self.members:
                if self.members[T] > 0 and T != self.target.tileID:
                    dist = Navigator.getDistance(self.target.tileID, T)
                    if dist > pre:
                        pre = dist
                        selected = T

            if selected is not None:
                self.members[selected] -= 1
                Environment.get().tiles[selected].available += 1
            else:
                error("Breaking reducing loop!")
                break

        return []

    def _hire(self) -> Tasks:
        """
        Hire new PODs for this Squad anywhere on the map

        This will make the PODs move towards the target regardless
        of anything around them and regardless of the chosen quest.
        """

        tasks = []
        env = Environment.get()
        for x in range(min(
                self.strength[0] - self.getStrength(),
                SQUAD_MAX_HIRE_PER_TURN
        )):

            # Try to find a valid zone that has unallocated members
            error(f"Moveable: {env.moves}")
            if len(env.moves) == 0:
                break
            try:
                counter = 0
                zone = random.choice(env.moves)
                while env.tiles[zone].available == 0:
                    zone = random.choice(env.moves)
                    counter += 1
                    if counter >= SQUAD_MAX_HIRE_TRIES:
                        raise RuntimeError
            except RuntimeError:
                pass
            error(f"Hiring. Zone ID: {zone}")

            # If there's at least one unallocated POD on the Tile,
            # we hire it and make it move towards the target
            if env.tiles[zone].available > 0:
                env.tiles[zone].available -= 1
                if zone not in self.members:
                    self.members[zone] = 0
                self.members[zone] += 1
                tasks.append(
                    self._movePODs(env.tiles[zone], self.target, 1)
                )

        return tasks

    def addMembers(self, n: int, src: Tile):
        """
        Add members to the Squad if they're available
        """

        if self.getStrength() + n > self.strength[1]:
            n = self.strength[1] - self.getStrength()
            error(f"Too much power. Reduced to {n} for {self}.")

        if n > src.available:
            n = src.available
            error(f"Not enaugh PODs available. Reduced to {n}.")

        if src.tileID not in self.members:
            self.members[src.tileID] = 0
        self.members[src.tileID] += n
        src.available -= n

    def turn(self) -> Tasks:
        """
        Calculate and return the moves of the Squad for one game turn

        Note that the Squad needs to handle the movements
        on its own. If a Squad moves one POD to another Tile,
        then the Squad must set this in its internal map as
        well as tell it to the outside world in the Tasks list.
        """

        if self.target is None:
            raise SquadNeedsTarget

        error(f"{self} performs a turn ...")

        tasks = []
        if self.getStrength() < self.strength[0]:
            error(f"{self} needs more PODs")
            tasks = self._hire()
        elif self.getStrength() > self.strength[1]:
            error(f"{self} has too many PODs")
            tasks = self._reduce()

        result = tasks + self._fulfill()
        error(f"Squad {self.squadID} performs: {result}")
        error(f"Squad {self.squadID} has members: {self.members}")
        return result


def defend(moves: Moveables) -> Tasks:
    """
    Plan a defense against incoming enemies while the rest continues exploring
    """

    # Start a loop over all tiles that have moveable PODs on them
    tasks = []
    for T in moves:
        tile = tiles[T]
        reachable = tile.links
        rest = tile.my

        # Enemies in the HQ are very dangerous,
        # so all upcoming PODs will stay in there
        if tile.tileID == myHQ and tile.enemy > 0:
            continue

        # Enemies in the HQ should be fight
        if myHQ in tile.links and tiles[myHQ].enemy > 0:
            plans = {myHQ: tile.my}
            rest = 0

        # Enemies around the HQ should be fight
        for target in reachable:
            if target in tiles[myHQ].links and tiles[target].enemy > 0 and rest > 0:
                plans = {target: tile.my}
                rest = 0

        # Otherwise explore normally
        if rest == tile.my:
            plans = explore(tile, reachable, False)

        # Use the plans dictionary to create the specific tasks
        for m in plans:
            task = [plans[m], tile.tileID, m]
            tasks.append(task)

    return tasks


def attack(moves: Moveables) -> Tasks:
    """
    Plan a full attack using the navigator dictionary
    """

    # Start a loop over all tiles that have moveable PODs on them
    tasks = []
    for T in moves:
        tile = tiles[T]
        reachable = tile.links
        plans = {}

        # Spread some PODs over all other targets
        targets = sortTargets(getTargets(reachable), tile.tileID)
        rest = tile.my - FULL_ATTACK_MIN_PODS
        while rest > 0:
            dest = random.choice(targets)
            if dest not in plans:
                plans[dest] = 0
            plans[dest] += 1
            rest -= 1

        # Use most of the other PODs for a tunneled attack
        if navi[T] not in plans:
            plans[navi[T]] = 0
        others = tile.my - sum(plans.values())
        plans[navi[T]] += others

        # Use the plans dictionary to create the specific tasks
        for m in plans:
            task = [plans[m], tile.tileID, m]
            #error("Task (attack):", task)
            tasks.append(task)

    return tasks


class Environment:
    _instance = None

    @classmethod
    def get(cls):
        """
        Return the (hopefully) only instance of the Environment
        """

        return cls._instance

    def __init__(
            self,
            players: int,
            zones: int,
            links: int,
            myID: int,
            enemyID: int,
            squads: typing.Dict[int, Squad] = {},
            tiles: typing.Dict[int, Tile] = {}
    ):
        if Environment._instance is None:
            Environment._instance = self

        def _genSquadID() -> typing.Generator[int, int, None]:
            squad = 0
            while True:
                yield squad
                squad += 1

        self.players = players      # number of players on the map
        self.zones = zones          # number of zones (size of the world)
        self.links = links          # number of total links between the zones
        self.myID = myID            # player ID of me
        self.enemyID = enemyID      # player ID of my oponent

        self.squads = squads        # dictionary of Squads
        self.tiles = tiles          # dictionary of Tiles

        self.lap: int = 0           # counter for the number of passed game turns
        self.money: int = 0         # always changing amount of money
        self.myHQ: int = -1         # tile ID of my HQ (set during first lap)
        self.enemyHQ: int = -1      # tile ID of the enemy HQ (set during first lap)
        self.neutralID: int = -1    # player ID of Gaia

        self.tasks: Tasks = []      # list of tasks to be performed in one game turn
        self.moves: Moveables = []  # list of zone IDs with moveable PODs (per turn)

        self.genID = _genSquadID()  # generator to calculate new squad IDs

    def _chooseNewQuestType(self, squadID: int, total: int, allocated: int) -> int:
        """
        Choose one of the quest types as new type
        """

        # TODO
        return 1  # defending  # TODO

        # Otherwise
        usedSquadTypes = {}
        for S in self.squads.values():
            if S.quest not in usedSquadTypes:
                usedSquadTypes[S.quest] = 0
            usedSquadTypes[S.quest] += 1

    def _makeNewSquad(self):
        """
        Create a new Squad if necessary
        """

        total = sum(T.my for T in self.tiles.values())
        allocated = sum(S.getStrength() for S in self.squads.values())
        error(f"PODs: total={total} allocated={allocated}")
        #if self.lap >= SQUAD_FIRST_BIRTHDAY and total - allocated > SQUAD_MIN_UNALLOCATED and not self.lap % 3:
        def create():
            squadID = next(self.genID)
            #strength = ((1, total - allocated // 2))
            strength = (7, total)
            quest = self._chooseNewQuestType(squadID, total, allocated)
            #target = self.tiles[random.randint(0, len(self.tiles) - 1)]
            #target = self.tiles[self.enemyHQ]
            target = self.tiles[self.myHQ + 1]
            error(f"New Squad {squadID} with quest {quest}, strength {strength} and target {target}")
            self.squads[squadID] = Squad(
                squadID, strength, quest, target = target
            )

        # FIXME
        if len(self.squads) == 0:
            create()

    def _manageDefendingSquad(self):
        """
        Start and manage the only defending Squad in the game
        """

        for S in self.squads.values():
            if S.quest == 1:
                # TODO: Add more PODs when the game goes on
                # TODO: Raise max strength to the max of all seen enemy PODs
                return
        else:
            squadID = next(self.genID)
            self.squads[squadID] = Squad(
                squadID, (1, 2), 1, target = self.tiles[self.myHQ]
            )

    def _getTargets(self, reachable: Moveables) -> Moveables:
        """
        Determine a list of good targets (or all reachable destinations)
        """

        targets = []
        for target in reachable:
            if self.tiles[target].player != self.myID:
                targets.append(target)

        if len(targets) == 0:
            targets = reachable
        return targets

    def _sortTargets(self, targets: Moveables, origin: Tile) -> Moveables:
        """
        Sort the list of targets based on their platinum, enemy count and state

        The list of targets should be a list of tile IDs.
        The origin should be the Tile where the POD came from.
        """

        def getKeyValue(t):
            """
            Calculate the sorting key value of a tile
            """

            T = self.tiles[t]
            value = COMP_BASE_VALUE

            # Add the value of the platinum of the tile
            value += T.platinum * COMP_PLATINUM_MULT

            # Give bonus points for enemies next to me
            if T.enemy < origin.my:
                value += COMP_WEAK_ENEMY
            elif T.enemy == origin.my:
                value += COMP_EQUAL_ENEMY
            elif T.enemy > origin.my:
                value += COMP_STRONG_ENEMY

            # Give bonus points for some sort of near tile
            if T.player == self.enemyID:
                value += COMP_ENEMY_TILE
            elif T.player == self.neutralID:
                value += COMP_NEUTRAL_TILE
            elif T.player == self.myID:
                value += COMP_MY_OWN_TILE
            elif T.my > 0:
                value += COMP_TEAM_MATES
            elif T.tileID == self.enemyHQ:
                value += COMP_ENEMY_HQ

            return value

        targets.sort(key = getKeyValue, reverse = True)
        return targets

    def _planExpand(
            self,
            tile: Tile,
            movers: int,
            randomized: bool = False
    ) -> typing.Dict[int, int]:
        """
        Plan an expanding game turn but don't perform actions

        This method does not change any attributes, it works
        read-only. The returned planning dict should be used
        to actually perform the plans. But note that some
        of those plans may not be possible, due to too few
        available PODs on the origin Tile. The parameter
        `movers` can be used to limit the movement plans.
        """

        # Create some necessary variables and get the targets
        plans = {}
        reachable = tile.links
        targets = self._sortTargets(self._getTargets(reachable), tile)
        if randomized:
            random.shuffle(targets)

        # In case we only have very few PODs, they should
        # always go to the best targets possible
        if movers == 1:
            plans[targets[0]] = 1
        elif movers == 2 and len(targets) >= 2:
            plans[targets[0]] = 1
            plans[targets[1]] = 1
        elif movers == 3 and len(targets) >= 3:
            plans[targets[0]] = 1
            plans[targets[1]] = 1
            plans[targets[2]] = 1
        elif len(targets) == 1:
            plans[targets[0]] = movers

        # In case there are two targets, the PODs should split
        # equally on them while preferring the first target
        elif len(targets) == 2:
            plans[targets[0]] = movers // 2
            plans[targets[1]] = movers // 2
            if movers % 2:
                plans[targets[0]] += 1

        # Otherwise they should also split on the available targets but they
        # should use a different number depending on the available PODs
        else:
            army = 1
            while movers > 0:
                for a in range(army):
                    dest = targets[a]
                    if movers > 0:
                        if dest not in plans:
                            plans[dest] = 0
                        plans[dest] += 1
                        movers -= 1
                if army < len(targets):
                    army += 1

        return plans

    def explore(self, tile: Tile, randomized: bool = False) -> typing.Dict[int, int]:
        """
        Plan a rapid exploration game turn
        """

        plans = {}
        generatedMovements = self._planExpand(tile, tile.available, randomized)
        for dst in generatedMovements:
            if generatedMovements[dst] <= tile.available:
                plans[dst] = generatedMovements[dst]
                tile.available -= generatedMovements[dst]
        return plans

    def plan(self):
        """
        Plan the next movements of all Squads and all other PODs
        """

        # If it seems like the HQ is getting attacked,
        # we must defend it under all circumstances
        if (sum(self.tiles[t].enemy for t in self.tiles[self.myHQ].links)
                + self.tiles[self.myHQ].enemy > 1):
            error("Enemies detected!")
            #self.tasks = self.defend()  # FIXME

        # Iterate over all Squads to plan their movements first
        for S in self.squads.values():
            self.tasks += S.turn()

        # Create a new Squad if it seems necessary
        self._makeNewSquad()

        # FIXME (Test)
        return

        # If the game is coming to an end, all Squads should
        # try attacking the enemy as hard as possible
        # FIXME
        #global FULL_ATTACK_CHANCE
        #if lap >= FULL_ATTACK or (random.random() < FULL_ATTACK_CHANCE and lap > RAPID_EXPLORING):
        #    error("Turn: full attack ({:.2f}%)".format(FULL_ATTACK_CHANCE))
        #    FULL_ATTACK_CHANCE = min(1, FULL_ATTACK_CHANCE + 0.1)
        #    self.tasks = attack(moves)

        # If there are only some game turns left, all Squads should
        # attack the enemy HQ as soon as possible
        if self.lap in range(*SQUAD_ALL_ATTACKING_LAPS):
            for S in self.squads.values():
                S.quest = 2
                S.target = self.tiles[self.enemyHQ]

        filterAllocated = lambda T: self.tiles[T].available > 0
        # Start a loop over all tiles that have moveable PODs on them
        for T in filter(filterAllocated, self.moves):
            tile = self.tiles[T]
            reachable = tile.links

            # Dictionary of moves where the key is a tile ID
            # and the value is the number of PODs going there
            plans = {}

            # Start with a rapid exploration at first or with some chance
            if self.lap <= RAPID_EXPLORING or random.random() < RAPID_EXPLORING_CHANCE:
                error(f"Turn: rapid exploring of {tile}")
                plans = self.explore(tile, False)

            # Use a more random method otherwise
            else:
                error(f"Turn: random behavior of {tile}")
                plans = self.explore(tile, True)

            # Use the plans dictionary to create the specific tasks
            for m in plans:
                task = [plans[m], tile.tileID, m]
                self.tasks.append(task)

    def main(self):
        """
        Execute the main loop of the program infinitively
        """

        # Start the main game loop infinitively
        error("Starting game loop ...")
        while True:
            self.lap += 1

            # Set up the non-static attributes for every game turn
            self.money = int(input())
            self.tasks = []
            self.moves = []

            # Get all the new information about the game state via standard input
            for i in range(self.zones):
                Z, owner, pods0, pods1, visible, platinum = map(int, input().split())

                # Only update the data of the Tile if it's visible
                if visible:
                    self.tiles[Z].explored = True
                    self.tiles[Z].player = owner
                    self.tiles[Z].visit()
                    self.tiles[Z].platinum = platinum

                    #error("Zone {} of {} has P0={} and P1={} with {}".format(
                    #    Z, owner, pods0, pods1, platinum
                    #))

                    # Set the correct number of PODs on every Tile
                    if self.myID == 0:
                        my, enemy = pods0, pods1
                    elif self.myID == 1:
                        enemy, my = pods0, pods1
                    self.tiles[Z].setPods(my, enemy)

                    # Get a list of moveable PODs and set my HQ in the first game turn
                    if my > 0:
                        self.moves.append(Z)
                        if self.lap == 1:
                            error(f"HQ of player {self.myID} (me) on zone {Z}")
                            self.myHQ = Z

                    # Set the enemy HQ in the first game turn
                    if self.lap == 1 and enemy > 0:
                        error(f"HQ of player {self.enemyID} (enemy) on zone {Z}")
                        self.enemyHQ = Z

            # Start and manage the only defending Squad
            self._manageDefendingSquad()

            # Update the map data of all Squads
            for S in self.squads.values():
                for T in S.members:
                    if S.members[T] > self.tiles[T].available:
                        S.members[T] = self.tiles[T].available
                    self.tiles[T].available -= S.members[T]

            # Plan all movements
            error("Planning...")
            self.plan()
            error("Finished planning!")

            # Print out the planned movements
            print(*[
                " ".join(map(str, task))
                for task in self.tasks
                if task[1] != task[2] and task[0] > 0
            ])
            print("WAIT")

            # Increment the lap counter on every Tile and every Squad
            for T in self.tiles.values():
                T.lap()
            for S in self.squads.values():
                S.lap()


class Navigator:
    _navi: typing.Dict[typing.Tuple[int, int], typing.List[int]] = {}

    @classmethod
    def navigate(cls, start: int, end: int, tiles: typing.Dict[int, Tile]) -> typing.List[int]:
        """
        Return a path from start to end (as list of Tile IDs)

        This classmethod should always be used over
        the background functions as it supports caching.
        Note that it does not support multiple environments!
        """

        cls.generate(start, end, tiles)
        return cls._navi[(start, end)]

    @classmethod
    def getDistance(cls, start: int, end: int, tiles: typing.Dict[int, Tile]) -> int:
        """
        Calculate the distance between two zones

        This classmethod should always be used over
        the background functions as it supports caching.
        Note that it does not support multiple environments!
        """

        return len(cls.navigate(start, end, tiles))

    @classmethod
    def generate(cls, start: int, end: int, tiles: typing.Dict[int, Tile]):
        """
        Generate and store the path from start to end and vice versa
        """

        if (start, end) not in cls._navi:
            path = cls._genNavi(start, end, tiles)
            cls._navi[(start, end)] = path
            cls._navi[(end, start)] = path

    @staticmethod
    def _genNavi(start: int, end: int, tiles: typing.Dict[int, Tile]) -> typing.List[int]:
        """
        Generate a path from start to end using back tracking

        The returned list contains all Tile IDs from start to
        end point (including them) along the path.
        """

        for T in tiles.values():
            T.seen = False
            T.parent = None

        q = queue.Queue()
        q.put(tiles[start])
        tiles[start].seen = True

        while not q.empty():
            v = q.get()
            if v.tileID == end:
                break
            for l in v.links:
                if not tiles[l].seen:
                    tiles[l].seen = True
                    tiles[l].parent = v.tileID
                    q.put(tiles[l])

        path = [end]
        z = end
        while z != start:
            z = tiles[z].parent
            if z is None:
                break
            path.append(z)
        return path[::-1]


def setup() -> Environment:
    """
    Produce a new Environment object interactively
    """

    # Get some basic information about the world
    players, myID, zones, links = map(int, input().split())
    enemyID = 1 if myID == 0 else 0
    error("Size of the map: {} zones and {} links".format(zones, links))

    # Set up the tiles map (world map)
    tiles: typing.Dict[int, Tile] = {}
    for i in range(zones):
        zoneID, platinum = map(int, input().split())
        tiles[zoneID] = Tile(zoneID, -1, platinum, [])
    for i in range(links):
        zone1, zone2 = map(int, input().split())
        tiles[zone1].add(zone2)
        tiles[zone2].add(zone1)

    # Create and return a new environment
    return Environment(
        players = players,
        zones = zones,
        links = links,
        myID = myID,
        enemyID = enemyID,
        squads = {},
        tiles = tiles
    )


if __name__ == "__main__":
    env = setup()
    env.main()
else:
    raise ImportError
