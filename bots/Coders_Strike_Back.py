import sys
import math

# This code automatically collects game data in an infinite loop.
# It uses the standard input to place data into the game variables such as x and y.
# YOU DO NOT NEED TO MODIFY THE INITIALIZATION OF THE GAME VARIABLES.

boosted = False

waypoints = []

lap = 1
lapSeen = False

loop = 0

# game loop
while True:
    # x: x position of your pod
    # y: y position of your pod
    # next_checkpoint_x: x position of the next check point
    # next_checkpoint_y: y position of the next check point
    inp1 = input()
    inp2 = input()

    loop += 1

    x, y, next_x, next_y, dist, angle = [int(i) for i in inp1.split()]
    opp_x, opp_y = map(int, inp2.split())

    if (next_x, next_y) not in waypoints:
        waypoints.append((next_x, next_y))

    index = waypoints.index((next_x, next_y))
    if index == 0 and len(waypoints) >= 2 and not lapSeen:
        lap += 1
        lapSeen = True
    elif index == 1 and len(waypoints) >= 2:
        lapSeen = False

    print("x={}, y={}, dist={}, angle={}".format(x, y, dist, angle), file = sys.stderr)
    print("i={}, list={}, lap={}".format(index, waypoints, lap), file = sys.stderr)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)
    thrust = 100

    if dist < 600:
        thrust = 30
    elif dist < 2400:
        thrust = dist / 24

    if angle > 90 or angle < -90:
        thrust = 0
    elif angle > 75 or angle < -75:
        thrust /= 10
    elif angle > 60 or angle < -60:
        thrust /= 2
    elif -10 < angle < 10 and thrust < 60:
        thrust = min(100, 2 * thrust)

    thrust = max(thrust, 10)

    if dist > 10000 and not boosted and -5 < angle < 5 and lap > 1:
        thrust = "BOOST"
        boosted = True



    # Edit this line to output the target position
    # and thrust (0 <= thrust <= 100)
    # i.e.: "x y thrust"
    #print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " 100")

    print("x={}, y={}, thrust={}".format(next_x, next_y, thrust), file = sys.stderr)

    if isinstance(thrust, (int, float)):
        print(*map(str, map(int, [next_x, next_y, thrust])))
    else:
        print(*map(str, map(int, [next_x, next_y])), thrust)

